=begin

==更新履歴
  Date
	17/7/12		新規：修正1,2,3追加1,2,3,4
	17/7/18		修正:追加4
	17/8/2		修正:追加4

==
	修正1:陸地に降りた際にそのまま無音でBGMが切り替わらない。
	修正2:連戦でオートステートが機能しない。
	修正3:二刀流時にダメージが異常に増加する。
	追加1:職業を忘れる
	追加2:マスタリーと二刀流
	追加3:混沌の迷宮エネミーのスキル使用台詞
	追加4:拡張特徴の計算式変更
=end

#修正1:陸地に降りた際にそのまま無音でBGMが切り替わらない。
#==============================================================================
# ■ Game_Vehicle
#==============================================================================
class Game_Vehicle < Game_Character
  #--------------------------------------------------------------------------
  # ● 乗り物から降りる
  #--------------------------------------------------------------------------
	alias nw_field_get_off get_off
  def get_off
    if $game_map.auto_bgm?
      @driving = false
      @walk_anime = false
      @step_anime = false
      @direction = 4
      Audio.reset_sound
      $game_map.autoplay_landing 
    else
      nw_field_get_off
    end
  end
end

#修正2:連戦でオートステートが機能しない。
#==============================================================================
# ■ Game_Battler
#==============================================================================
class Game_Battler < Game_BattlerBase
	def on_battle_end
    remove_battle_states
    remove_all_buffs
    clear_actions
    init_tp unless preserve_tp?
    appear
    self.hp += Integer(mhp * battle_end_heel_hp)
    self.mp += Integer(mmp * battle_end_heel_mp)
    @predationed = false
		@result.clear
	end
end

#追加1:職業を忘れる
#==============================================================================
# ■ Game_Interpreter
#==============================================================================
class Game_Interpreter
  #--------------------------------------------------------------------------
  # ● 職業（種族）を忘れる
  #--------------------------------------------------------------------------
	def erase_class_level(actor_id, class_id)
		return unless $game_actors.exist?(actor_id)
		$game_actors[actor_id].erase_class(class_id)
	end
end

#==============================================================================
# ■ Game_Actor
#==============================================================================
class Game_Actor < Game_Battler
	#--------------------------------------------------------------------------
  # ● 職業（種族）を忘れる
  #--------------------------------------------------------------------------
	def erase_class(class_id)
		return unless @level_list.key?(class_id)
		kind = NWConst::Class::JOB_RANGE.include?(class_id) ? :class : :tribe
		@exp[class_id] = 0
		@level_list.delete(class_id) 
		current_class_id = kind == :class ? @class_id : @tribe_id
		if current_class_id == class_id
		@level_list[class_id] = 1
		end 
	end
end

#追加2:マスタリーと二刀流
#==============================================================================
# ■ RPG::BaseItem
#==============================================================================
class RPG::BaseItem
  #--------------------------------------------------------------------------
  # ● メモ欄解析処理
  #--------------------------------------------------------------------------
  def nw_note_analyze
    nw_kure_base_item_note_analyze
       
    self.note.each_line do |line|
      if NWRegexp::BaseItem::FEATURE_XPARAM_EX.match(line)
        array = [:命中, :回避, :会心, :会心回避, :魔法回避, :魔法反射, :反撃, :HP再生, :MP再生, :TP再生]
        @add_features.push(RPG::BaseItem::Feature.new(FEATURE_XPARAM_EX, array.index($1.to_sym), $2.to_f * 0.01))
      elsif NWRegexp::BaseItem::PARTY_ABILITY.match(line)
        kind = [:獲得金額, :獲得アイテム, :エンカウント, :仲間加入]
        kind_id = kind.index($1.to_sym)
        @add_features.push(RPG::BaseItem::Feature.new(FEATURE_PARTY_EX_ABILITY, kind_id, $2.to_f * 0.01))
      elsif NWRegexp::BaseItem::SLOT_CHANCE.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(FEATURE_PARTY_EX_ABILITY, SLOT_CHANCE, $1.to_i))
      elsif NWRegexp::BaseItem::UNLOCK_LEVEL.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(FEATURE_PARTY_EX_ABILITY, UNLOCK_LEVEL, $1.to_i))
      elsif NWRegexp::BaseItem::STEAL_SUCCESS.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, STEAL_SUCCESS, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::GET_EXP_RATE.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, GET_EXP_RATE, $1.to_f * 0.01))        
      elsif NWRegexp::BaseItem::GET_CLASSEXP_RATE.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, GET_CLASSEXP_RATE, $1.to_f * 0.01))        
      elsif NWRegexp::BaseItem::AUTO_STAND.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, AUTO_STAND, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::HEEL_REVERSE.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, HEEL_REVERSE, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::AUTO_STATE.match(line)
        array = []
        $1.split(/\,\s?/).each{|id|array.push(id.to_i)}
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, AUTO_STATE, array))
      elsif NWRegexp::BaseItem::TRIGGER_STATE.match(line)
        hash = {:point => $1.to_sym, :trigger => $2.to_i, :per => $3.to_f * 0.01, :state_id => $4.to_i}
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, TRIGGER_STATE, hash))
      elsif NWRegexp::BaseItem::METAL_BODY.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, METAL_BODY, $1.to_i))
      elsif NWRegexp::BaseItem::DEFENSE_WALL.match(line)  
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, DEFENSE_WALL, $1.to_i))
      elsif NWRegexp::BaseItem::INVALIDATE_WALL.match(line)  
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, INVALIDATE_WALL, $1.to_i))
      elsif NWRegexp::BaseItem::DAMAGE_MP_CONVERT.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, DAMAGE_MP_CONVERT, $1.to_f * 0.01))          
      elsif NWRegexp::BaseItem::DAMAGE_GOLD_CONVERT.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, DAMAGE_GOLD_CONVERT, $1.to_f * 0.01))          
      elsif NWRegexp::BaseItem::DAMAGE_MP_DRAIN.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, DAMAGE_MP_DRAIN, $1.to_f * 0.01))          
      elsif NWRegexp::BaseItem::DAMAGE_GOLD_DRAIN.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, DAMAGE_GOLD_DRAIN, $1.to_f * 0.01))          
      elsif NWRegexp::BaseItem::DEAD_SKILL.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, DEAD_SKILL, $1.to_i))
      elsif NWRegexp::BaseItem::BATTLE_START_SKILL.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, BATTLE_START_SKILL,
          {:id => $1.to_i, :per => $2.to_f * 0.01}))
      elsif NWRegexp::BaseItem::TURN_START_SKILL.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, TURN_START_SKILL,
          {:id => $1.to_i, :per => $2.to_f * 0.01}))
      elsif NWRegexp::BaseItem::TURN_END_SKILL.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, TURN_END_SKILL,
          {:id => $1.to_i, :per => $2.to_f * 0.01}))
      elsif NWRegexp::BaseItem::CHANGE_ACTION.match(line)
        array = []
        $1.scan(/(\d+)\-(\d+)/){|a, b| array.push({:id => a.to_i, :per => b.to_f * 0.01})}
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, CHANGE_ACTION, array))
      elsif NWRegexp::BaseItem::STYPE_COST_RATE.match(line)  
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, STYPE_COST_RATE, {:type => $1.to_sym, :id => $2.to_i, :rate => $3.to_f * 0.01}))
      elsif NWRegexp::BaseItem::SKILL_COST_RATE.match(line)  
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, SKILL_COST_RATE, {:type => $1.to_sym, :id => $2.to_i, :rate => $3.to_f * 0.01}))
      elsif NWRegexp::BaseItem::TP_COST_RATE.match(line)  
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, TP_COST_RATE, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::HP_COST_RATE.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, HP_COST_RATE, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::GOLD_COST_RATE.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, GOLD_COST_RATE, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::INCREASE_TP.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, INCREASE_TP, {:plus => $1.to_s == "増加", :num => $2.to_i, :per => $3 ? true : false}))
      elsif NWRegexp::BaseItem::START_TP_RATE.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, START_TP_RATE, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::BATTLE_END_HEEL_HP.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, BATTLE_END_HEEL_HP, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::BATTLE_END_HEEL_MP.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, BATTLE_END_HEEL_MP, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::NORMAL_ATTACK.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, Battler::NORMAL_ATTACK, $1.to_i))          
      elsif NWRegexp::BaseItem::COUNTER_SKILL.match(line)
        array = []
        $1.split(/\,\s?/).each{|id| array.push(id.to_i)}
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, COUNTER_SKILL, array))
      elsif NWRegexp::BaseItem::FINAL_INVOKE.match(line)  
        array = []
        $1.split(/\,\s?/).each{|id| array.push(id.to_i)}
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, FINAL_INVOKE, array))
      elsif NWRegexp::BaseItem::CERTAIN_COUNTER.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, CERTAIN_COUNTER, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::MAGICAL_COUNTER.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, MAGICAL_COUNTER, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::CERTAIN_COUNTER_EX.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, CERTAIN_COUNTER_EX, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::PHYSICAL_COUNTER_EX.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, PHYSICAL_COUNTER_EX, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::MAGICAL_COUNTER_EX.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, MAGICAL_COUNTER_EX, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::CONSIDERATE.match(line)  
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, CONSIDERATE, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::INVOKE_REPEATS_TYPE.match(line)
        hash = {}
        $1.scan(/(\d+)\-(\d+)/){|a, b| hash[a.to_i] = b.to_i}
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, INVOKE_REPEATS_TYPE, hash))
      elsif NWRegexp::BaseItem::INVOKE_REPEATS_SKILL.match(line)
        hash = {}
        $1.scan(/(\d+)\-(\d+)/){|a, b| hash[a.to_i] = b.to_i}
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, INVOKE_REPEATS_SKILL, hash))
      elsif NWRegexp::BaseItem::OWN_CRUSH_RESIST.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, OWN_CRUSH_RESIST, true))        
      elsif NWRegexp::BaseItem::ELEMENT_DRAIN.match(line)
        array = []
        $1.split(/\,\s?/).each{|id| array.push(id.to_i)}
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, ELEMENT_DRAIN, array))
      elsif NWRegexp::BaseItem::IGNORE_OVER_DRIVE.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, IGNORE_OVER_DRIVE, true))        
      elsif NWRegexp::BaseItem::INSTANT_DEAD_REVERSE.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, INSTANT_DEAD_REVERSE, true))
      elsif NWRegexp::BaseItem::CHANGE_SKILL.match(line)
        hash = {}
        $1.scan(/(\d+)\-(\d+)/){|a, b| hash[a.to_i] = b.to_i}
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, CHANGE_SKILL, hash))
      elsif NWRegexp::BaseItem::ITEM_COST_SCRIMP.match(line)
        hash = {}
        $1.scan(/(\d+)\-(\d+)/){|a, b| hash[a.to_i] = b.to_f * 0.01}
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, ITEM_COST_SCRIMP, hash))
      elsif NWRegexp::BaseItem::NEED_ITEM_IGNORE.match(line)
        array = []
        $1.split(/\,\s?/).each{|id| array.push(id.to_i)}          
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, NEED_ITEM_IGNORE, array))
      elsif NWRegexp::BaseItem::MULTI_BOOSTER.match(line)
        kind = [
          :属性強化,
          :武器強化物理,
          :武器強化魔法,
          :武器強化必中,
          :通常攻撃強化,
          :ステート割合強化タイプ,
          :ステート固定強化タイプ,
          :スキルタイプ強化,
          :ステート割合強化スキル,
          :スキル強化
        ]
        kind_id = kind.index($1.to_sym)
        hash = {}
        $2.scan(/(\d+)\-(\d+)/){|a, b| hash[a.to_i] = b.to_f * 0.01}
        @add_features.push(RPG::BaseItem::Feature.new(FEATURE_MULTI_BOOSTER, kind_id, hash))
      elsif NWRegexp::BaseItem::WTYPE_SKILL_BOOST.match(line)  
        hash = {}
        $1.scan(/(\d+)\-(\d+)\-(\d+)/){|a, b, c| hash[[a.to_i, b.to_i]] = c.to_f * 0.01}
        @add_features.push(RPG::BaseItem::Feature.new(FEATURE_MULTI_BOOSTER, WTYPE_SKILL, hash))
      elsif NWRegexp::BaseItem::COUNTER_BOOST.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(FEATURE_MULTI_BOOSTER, COUNTER, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::FALL_HP_BOOST.match(line)
        hash = {:per => $1.to_f * 0.01, :boost => $2.to_f * 0.01}
        @add_features.push(RPG::BaseItem::Feature.new(FEATURE_MULTI_BOOSTER, FALL_HP, hash))
      elsif NWRegexp::BaseItem::OVER_SOUL.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_MULTI_BOOSTER, OVER_SOUL, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::DUMMY_ENCHANT.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(FEATURE_DUMMY_ENCHANT, nil, $1.to_s))
      elsif NWRegexp::BaseItem::TERRAIN_BOOSTER.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_TERRAIN_BOOSTER, $1.to_sym, $2.nil? ? 0.2 : 0.4))
      elsif NWRegexp::BaseItem::SKILL_CONVERT_PARAM.match(line)
        @data_ex[:skill_convert_param_data] ||= Hash.new
        @data_ex[:skill_convert_param_data][$1.to_i] ||= []
        @data_ex[:skill_convert_param_data][$1.to_i].push([$2.to_i + 1, $3.to_i + 1])
      elsif NWRegexp::BaseItem::PHYSICAL_REFLECTION.match(line)
        @add_features.push(RPG::BaseItem::Feature.new(
          FEATURE_BATTLER_ABILITY, PHYSICAL_REFLECTION, $1.to_f * 0.01))
      elsif NWRegexp::BaseItem::SELLD_DRAW.match(line)
        @data_ex[:selld_draw] = $1.to_s
      elsif NWRegexp::BaseItem::EXCLUDE.match(line)
        @data_ex[:lib_exclude?] = true
      elsif NWRegexp::BaseItem::EQUIP_MASTERY.match(line)
        category = [:武器, :防具].index($1.to_sym)
        $2.scan(/(\d+)\-(\d+)/) {|a, b|
          @add_features.push(RPG::BaseItem::Feature.new(
            FEATURE_EQUIP_MASTERY, [category, a.to_i], b.to_i * 0.01))
        }
      end
    end
  end
end

#追加3:混沌の迷宮エネミーのスキル使用台詞
#==============================================================================
# ■ Game_Battler
#==============================================================================
class Game_Battler < Game_BattlerBase
	#--------------------------------------------------------------------------
  # ● スキルセリフデータ格納ハッシュの取得
  #--------------------------------------------------------------------------  
  def skill_word_hash
		if actor? 
			data = NWConst::Actor::SKILL_WORDS
			return data.key?(self.id) ? data[self.id] : nil
		else
			data = NWConst::Enemy::SKILL_WORDS
			id = $data_enemies[self.id].ex_dungeon_enemy? ? self.id-1000 : self.id 
			return data.key?(id) ? data[id] : nil
		end
  end
  #--------------------------------------------------------------------------
  # ● ダウンセリフデータ格納ハッシュの取得
  #--------------------------------------------------------------------------  
  def down_word_hash
		if actor? 
			data = NWConst::Actor::DOWN_WORDS
			return data.key?(self.id) ? data[self.id] : nil
		else
			data = NWConst::Enemy::DOWN_WORDS
			id = $data_enemies[self.id].ex_dungeon_enemy? ? self.id-1000 : self.id 
			return data.key?(id) ? data[id] : nil
		end
  end
end 

#追加4:拡張特徴計算式変更
#==============================================================================
# ■ Game_BattlerBase
#==============================================================================
class Game_BattlerBase
  #--------------------------------------------------------------------------
  # ● スキルタイプ消費率を取得
  #--------------------------------------------------------------------------
  def stype_cost_rate(stype_id, type)
    features_with_id(FEATURE_BATTLER_ABILITY, STYPE_COST_RATE).select{|ft|
      ft.value[:type] == type
    }.inject(1.0){|r, ft|
      ft.value[:id] == stype_id ? [r * ft.value[:rate] ,0.01].max : r   
    }
  end  
  #--------------------------------------------------------------------------
  # ● スキル消費率を取得
  #--------------------------------------------------------------------------
  def skill_cost_rate(skill_id, type)
    features_with_id(FEATURE_BATTLER_ABILITY, SKILL_COST_RATE).select{|ft|
      ft.value[:type] == type
    }.inject(1.0){|r, ft|
      ft.value[:id] == skill_id ? [r * ft.value[:rate] ,0.01].max : r       
    }
  end  
  #--------------------------------------------------------------------------
  # ● TP消費率を取得
  #--------------------------------------------------------------------------
  def tp_cost_rate
    features_pi(FEATURE_BATTLER_ABILITY, TP_COST_RATE)
  end  
  #--------------------------------------------------------------------------
  # ● HP消費率を取得
  #--------------------------------------------------------------------------
  def hp_cost_rate
    features_pi(FEATURE_BATTLER_ABILITY, HP_COST_RATE)
  end  
  #--------------------------------------------------------------------------
  # ● 連続発動を取得
  #--------------------------------------------------------------------------
  def invoke_repeats(item)
    return 1 unless item.is_skill?
    stype_count = item.stypes.inject(1){|count, stype_id|
      features_with_id(FEATURE_BATTLER_ABILITY, INVOKE_REPEATS_TYPE).each{|ft|
        if ft.value.key?(stype_id)
          count += ft.value[stype_id] - 1
        end
      }
      count
    }
    skill_count = features_with_id(FEATURE_BATTLER_ABILITY, INVOKE_REPEATS_SKILL).inject(1){|count, ft|
      if ft.value.key?(item.id) then count += ft.value[item.id] -1  else count end
    }
    return [stype_count, skill_count].max
  end
  #--------------------------------------------------------------------------
  # ● 指定アイテムに適用するマスタリー倍率を取得
  #--------------------------------------------------------------------------
  def item_mastery_rate(item)
    ft_id = nil
    ft_id = [0, item.wtype_id] if item.is_a?(RPG::Weapon)
    ft_id = [1, item.atype_id] if item.is_a?(RPG::Armor)
		rate = features_max(NWFeature::FEATURE_EQUIP_MASTERY, ft_id)
		rate = 1.0 if rate == 0
    return rate
  end
end

#修正3:二刀流時にダメージが異常に増加する。
#==============================================================================
# ■ Game_Battler
#==============================================================================
class Game_Battler < Game_BattlerBase
	#--------------------------------------------------------------------------
  # ● ブースター補正率の取得
  #--------------------------------------------------------------------------
  def boost_rate(user, item, is_cnt)
    value  = 1.0
    value *= user.final_elements(item).inject(1.0){|max, id| max = max > user.booster_element(id) ? max : user.booster_element(id)}
    value *= 1.0 + (user.friends_unit.dead_members.size * user.considerate)
    value *= 1.0 + (user.friends_unit.dead_members.size * item.considerate_revise)
    wrate = user.wtypes.map{|id| item.weapon_rate(id)}.max
    value *= wrate if wrate
    value *= user.pha if item.apply_pharmacology?
    value *= user.booster_counter if is_cnt
    
    user.wtypes.each do |wtype_id|
      case item.hit_type
      when 0; value *= user.booster_weapon_certain(wtype_id)
      when 1; value *= user.booster_weapon_physical(wtype_id)
      when 2; value *= user.booster_weapon_magical(wtype_id)
      end
    end
    if item.is_skill?
      use_wtypes = user.wtypes.empty? ? [0] : user.wtypes
			values = []
      use_wtypes.each do |wtype_id|
				t_value = value 
        t_value *= user.booster_wtype_skill(wtype_id, item)
        if item == $data_skills[user.attack_skill_id]
          t_value *= user.booster_normal_attack(wtype_id)
        end
				values << t_value
      end
			value = values.max
      value *= user.booster_skill_type(item)
      value *= user.booster_skill(item)
    end
    return value
  end
end